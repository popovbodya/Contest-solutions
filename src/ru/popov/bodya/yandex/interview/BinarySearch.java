package ru.popov.bodya.yandex.interview;

import java.util.Arrays;

public class BinarySearch {

    public static void main(String[] args) {
        final long[] arr = new long[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 16, 20, 41, 50, 55, 65, 75, 85, 95, 105, 115};
        System.out.println(binarySearchLong(arr, 30));

    }

    private static int binarySearchLong(long[] arr, long key) {
        int left = 0;
        int right = arr.length - 1;

        while (right - left > 1) {
            int middle = left + (right - left) / 2;
            if (arr[middle] >= key) {
                right = middle;
            } else {
                left = middle;
            }
        }
        return right;
    }

}
