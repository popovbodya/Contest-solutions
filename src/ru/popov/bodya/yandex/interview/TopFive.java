package ru.popov.bodya.yandex.interview;

import java.util.*;

public class TopFive {

    private static final int SIZE = 5;

    public static void main(String[] args) {

        final Scanner scanner = new Scanner(System.in);
        final int size = scanner.nextInt();
        final List<Integer> list = new ArrayList<>();
        final Queue<Integer> priorityQueue = new PriorityQueue<>(SIZE + 1);

        for (int i = 0; i < size; i++) {
            final int nextValue = scanner.nextInt();

            priorityQueue.add(nextValue);
            if (priorityQueue.size() > SIZE) {
                priorityQueue.poll();
            }
            list.addAll(priorityQueue);
            list.sort(Collections.reverseOrder());
            for (Integer integer : list) {
                System.out.print(integer + " ");
            }
            list.clear();
            System.out.println();
        }
    }

}
