package ru.popov.bodya.yandex.interview;

import java.util.Arrays;

public class BubbleSort {

    public static void main(String[] args) {
        BubbleSort bubbleSort = new BubbleSort();
        final int[] arr = {2, 1, 7, 3, 5, 15, 23, 7, 53, 60, 26, 14};
        bubbleSort.sort(arr);
        Arrays.stream(arr).forEach(value -> System.out.print(value + " "));
    }

    private void sort(final int[] arr) {
        for (int i = arr.length - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] > arr[j + 1]) {
                    swap(arr, j, j + 1);
                }
            }
        }
    }

    private void swap(final int[] arr, final int i, final int j) {
        final int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }
}
