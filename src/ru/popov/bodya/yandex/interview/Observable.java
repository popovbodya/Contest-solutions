package ru.popov.bodya.yandex.interview;

import java.util.ArrayList;
import java.util.List;

public class Observable {

    private final List<Observer> mObservers;

    public Observable() {
        mObservers = new ArrayList<>();
    }

    public synchronized void subscribe(Observer observer) {
        mObservers.add(observer);
    }

    public synchronized void unSubscribe(Observer observer) {
        mObservers.remove(observer);
    }

    public void fireAll() {
        final List<Observer> observersList;
        synchronized(this) {
            observersList = new ArrayList<>(mObservers);
        }
        for (Observer obs: observersList) {
            obs.fire(this);
        }
    }

    interface Observer {
        void fire(Observable observable);
    }

    class ObserverImpl implements Observer {

        @Override
        public void fire(Observable observable) {
            observable.unSubscribe(this);
        }
    }
}

