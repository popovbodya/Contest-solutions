package ru.popov.bodya.yandex.interview;

import java.util.Arrays;

public class QuickSort {

    public static void main(String[] args) {
        QuickSort quickSort = new QuickSort();
        final int[] arr = {2, 1, 7, 3, 5, 15, 23, 7, 53, 60, 26, 14};
        quickSort.quickSort(arr, 0, arr.length - 1);
        Arrays.stream(arr).forEach(value -> System.out.print(value + " "));
    }

    private int partition(int arr[], int left, int right) {

        int i = left, j = right;
        int tmp;
        int pivot = arr[(left + right) / 2];

        while (i <= j) {

            while (arr[i] < pivot) {
                i++;
            }

            while (arr[j] > pivot) {
                j--;
            }

            if (i <= j) {
                tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
                i++;
                j--;
            }

        }
        return i;
    }

    private void quickSort(int arr[], int left, int right) {
        int index = partition(arr, left, right);
        if (left < index - 1) {
            quickSort(arr, left, index - 1);
        }
        if (index < right) {
            quickSort(arr, index, right);
        }
    }

}
