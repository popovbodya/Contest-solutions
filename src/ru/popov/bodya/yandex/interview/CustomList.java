package ru.popov.bodya.yandex.interview;

import java.util.Scanner;

public class CustomList {
    public static void main(String[] args) {

        final Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        Node head = null;
        while (n > 0) {
            head = new Node(n, head);
            n--;
        }

        printList(head);
        final Node node = reverseList(head);
        printList(node);
    }

    private static void printList(Node head) {
        while (head != null) {
            System.out.print(head.data + " ");
            head = head.next;
        }
        System.out.println();
    }

    private static Node reverseList(Node head) {

        Node current = head;
        Node reversed = null;
        while (current != null) {
            Node next = current.next;
            current.next = reversed;
            reversed = current;
            current = next;
        }

        return reversed;
    }

    private static class Node {
        private final long data;
        private Node next;

        Node(long data, Node link) {
            this.data = data;
            next = link;
        }
    }
}
