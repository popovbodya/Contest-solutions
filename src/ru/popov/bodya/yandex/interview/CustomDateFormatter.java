package ru.popov.bodya.yandex.interview;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomDateFormatter {

    public static void main(String[] args) {
        final Date date = new Date(1530604943760L);
        final DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy 'at' HH:mm:ss z");
        final String dateFormatted = formatter.format(date);
        System.out.println(dateFormatted);
    }
}
