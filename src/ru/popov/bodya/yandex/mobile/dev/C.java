package ru.popov.bodya.yandex.mobile.dev;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

public class C {

    public static void main(String[] args) {
        final InputStream inputStream = System.in;
        final OutputStream outputStream = System.out;
        final InputReader in = new InputReader(inputStream);
        final PrintWriter out = new PrintWriter(outputStream);
        final TaskC solver = new TaskC();
        solver.solve(in, out);
        out.close();
    }

    private static class TaskC {

        private void solve(InputReader in, PrintWriter out) {
            final int n = in.nextInt();
            final Set<Integer> numbers = new HashSet<>();

            for (int i = 0; i < n; i++) {
                numbers.add(in.nextInt());
            }
            System.out.println(numbers.size());
        }
    }

    private static class InputReader {

        private static final int BUF_SIZE = 32768;
        private BufferedReader mBufferedReader;
        private StringTokenizer mTokenizer;

        private InputReader(InputStream stream) {
            mBufferedReader = new BufferedReader(new InputStreamReader(stream), BUF_SIZE);
            mTokenizer = null;
        }

        private String next() {
            while (mTokenizer == null || !mTokenizer.hasMoreTokens()) {
                try {
                    mTokenizer = new StringTokenizer(mBufferedReader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return mTokenizer.nextToken();
        }

        private int nextInt() {
            return Integer.parseInt(next());
        }

        private long nextLong() {
            return Long.parseLong(next());
        }
    }
}
