package ru.popov.bodya.yandex.mobile.dev;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class B {

    public static void main(String[] args) {
        final InputStream inputStream = System.in;
        final OutputStream outputStream = System.out;
        final InputReader in = new InputReader(inputStream);
        final PrintWriter out = new PrintWriter(outputStream);
        final TaskB solver = new TaskB();
        solver.solve(in, out);
        out.close();
    }

    private static class TaskB {

        private static final int BASE = 2;
        private static final int ZERO_COUNT = 1;

        private void solve(InputReader in, PrintWriter out) {
            final long number = in.nextLong();
            out.println(count(number));
            out.println((long)countWithLog(number));

        }

        private static long count(long number) {

            long count = 0;
            double tmp = 0;

            while (tmp <= number) {
                tmp = Math.pow(BASE, count++);
            }
            return count - 1;
        }

        private static double countWithLog(long number) {
            if (number == 0) {
                return 0;
            }
            return Math.floor(Math.log(number) / Math.log(BASE)) + ZERO_COUNT;
        }

    }

    private static class InputReader {

        private static final int BUF_SIZE = 32768;
        private BufferedReader mBufferedReader;
        private StringTokenizer mTokenizer;

        private InputReader(InputStream stream) {
            mBufferedReader = new BufferedReader(new InputStreamReader(stream), BUF_SIZE);
            mTokenizer = null;
        }

        private String next() {
            while (mTokenizer == null || !mTokenizer.hasMoreTokens()) {
                try {
                    mTokenizer = new StringTokenizer(mBufferedReader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return mTokenizer.nextToken();
        }

        private int nextInt() {
            return Integer.parseInt(next());
        }

        private long nextLong() {
            return Long.parseLong(next());
        }
    }
}
