package ru.popov.bodya.yandex.mobile.dev;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class A {

    public static void main(String[] args) {
        final InputStream inputStream = System.in;
        final OutputStream outputStream = System.out;
        final InputReader in = new InputReader(inputStream);
        final PrintWriter out = new PrintWriter(outputStream);
        final TaskA solver = new TaskA();
        solver.solve(in, out);
        out.close();
    }

    private static class TaskA {

        private void solve(InputReader in, PrintWriter out) {

            final int n = in.nextInt();
            final int m = in.nextInt();
            final int[][] array = new int[n][m];
            final int[] minsInColumn = new int[m];
            int count = 0;

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    array[i][j] = in.nextInt();
                }
            }

            for (int i = 0; i < m; i++) {
                minsInColumn[i] = min(column(array, i));
            }

            for (int i = 0; i < n; i++) {
                final List<Integer> minValuesIndexes = minValueIndexes(array[i]);
                final int min = !minValuesIndexes.isEmpty() ? array[i][minValuesIndexes.get(0)] : Integer.MAX_VALUE;
                for (Integer index : minValuesIndexes) {
                    if (min == minsInColumn[index]) {
                        count++;
                    }
                }
            }

            out.println(count);
        }

        private static List<Integer> minValueIndexes(int[] array) {

            final List<Integer> minValues = new ArrayList<>();
            int minValue = Integer.MAX_VALUE;

            for (int i = 0; i < array.length; i++) {
                final int elem = array[i];

                if (elem == minValue) {
                    minValues.add(i);
                }

                if (elem < minValue) {
                    minValue = elem;
                    minValues.clear();
                    minValues.add(i);
                }
            }
            return minValues;
        }

        private static int[] column(int[][] mas, int index) {
            final int size = mas.length;
            final int[] column = new int[size];
            for (int i = 0; i < size; i++) {
                column[i] = mas[i][index];
            }
            return column;
        }


        private static int min(int[] array) {
            int minValue = Integer.MAX_VALUE;
            for (int elem : array) {
                if (elem < minValue) {
                    minValue = elem;
                }
            }
            return minValue;
        }

    }

    private static class InputReader {

        private static final int BUF_SIZE = 32768;
        private BufferedReader mBufferedReader;
        private StringTokenizer mTokenizer;

        private InputReader(InputStream stream) {
            mBufferedReader = new BufferedReader(new InputStreamReader(stream), BUF_SIZE);
            mTokenizer = null;
        }

        private String next() {
            while (mTokenizer == null || !mTokenizer.hasMoreTokens()) {
                try {
                    mTokenizer = new StringTokenizer(mBufferedReader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return mTokenizer.nextToken();
        }

        private int nextInt() {
            return Integer.parseInt(next());
        }

        private long nextLong() {
            return Long.parseLong(next());
        }
    }
}
