package ru.popov.bodya.yandex.mobile.dev;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

public class D {

    public static void main(String[] args) {
        OutputStream outputStream = System.out;
        PrintWriter out = new PrintWriter(outputStream);
        TaskD solver = new TaskD();
        solver.solve(out);
        out.close();
    }

    private static class TaskD {

        private static class Pair {
            private final int start;
            private final int end;

            private Pair(int start, int end) {
                this.start = start;
                this.end = end;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Pair pair = (Pair) o;
                return start == pair.start &&
                        end == pair.end;
            }

            @Override
            public int hashCode() {
                return Objects.hash(start, end);
            }
        }

        private void solve(PrintWriter out) {

            final Scanner scanner = new Scanner(System.in);
            final Map<Pair, Integer> sessions = new HashMap<>();
            final int size = Integer.valueOf(scanner.nextLine());

            for (int i = 0; i < size; i++) {
                final String[] values = scanner.nextLine().split(" ");
                final Integer start = Integer.valueOf(values[0]);
                final Integer end = Integer.valueOf(values[1]);
                sessions.put(new Pair(start, end), 1);
            }

            out.println(compute(sessions));

        }

        private int compute(Map<Pair, Integer> sessions) {
            final Set<Pair> sessionKeySet = sessions.keySet();
            for (Pair session : sessionKeySet) {
                for (Pair comparingSession : sessionKeySet) {
                    if (session.equals(comparingSession)) {
                        continue;
                    }
                    if (session.start >= comparingSession.start && session.start <= comparingSession.end) {
                        sessions.compute(session, (pair, integer) -> integer == null ? 1 : integer + 1);
                    }
                }
            }

            return sessions
                    .entrySet()
                    .stream()
                    .filter(e -> e.getValue() == getCounter(sessions))
                    .min(Comparator.comparingInt(o -> o.getKey().start))
                    .map(entry -> entry.getKey().start)
                    .get();
        }

        private static int getCounter(Map<Pair, Integer> sessions) {
            return sessions
                    .entrySet()
                    .stream()
                    .max(Comparator.comparing(Map.Entry::getValue))
                    .get()
                    .getValue();
        }
    }
}

