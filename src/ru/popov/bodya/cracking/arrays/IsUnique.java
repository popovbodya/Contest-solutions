package ru.popov.bodya.cracking.arrays;


import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Implement an algorithm to determine if a string has all unique characters. What if you
 * cannot use additional data structures?
 */
public class IsUnique {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        final String string = scanner.next();
        System.out.println(isUniqueWithAdditionalData(string));
        System.out.println(isUniqueWithSort(string));
        System.out.println(isUniqueWithBitmask(string));
    }

    // We will assume that the string only uses the lowercase letters a through z.
    private static boolean isUniqueWithBitmask(final String string) {
        int checker = 0;
        for (int i = 0; i < string.length(); i++) {
            int val = string.charAt(i) - 'a';
            if ((checker & (1 << val)) > 0) {
                return false;
            }
            checker |= (1 << val);
        }
        return true;
    }

    private static boolean isUniqueWithSort(final String string) {
        final char[] chars = string.toCharArray();
        Arrays.sort(chars);
        for (int i = 0; i < chars.length - 1; i++) {
            if (chars[i] == chars[i + 1]) {
                return false;
            }
        }
        return true;
    }

    private static boolean isUniqueWithAdditionalData(final String string) {
        HashMap<Character, Boolean> isCharExistsMap = new HashMap<>();
        final char[] chars = string.toCharArray();
        for (char ch : chars) {
            if (isCharExistsMap.containsKey(ch)) {
                return false;
            } else {
                isCharExistsMap.put(ch, true);
            }
        }
        return true;
    }
}
