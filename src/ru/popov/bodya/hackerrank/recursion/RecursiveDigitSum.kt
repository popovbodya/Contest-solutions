package ru.popov.bodya.hackerrank.recursion

import java.util.*

fun superDigit(n: String, k: Int): Int {
    if (n.length == 1) {
        return n.toInt()
    }
    var sum: Long = 0
    for (i in 0 until n.length) {
        sum += Character.digit(n[i], 10)
    }
    sum *= k
    return superDigit(sum.toString(), 1)

}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    val nk = scan.nextLine().split(" ")
    val n = nk[0]
    val k = nk[1].trim().toInt()
    val result = superDigit(n, k)
    println(result)
}