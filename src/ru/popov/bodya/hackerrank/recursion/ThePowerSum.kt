package ru.popov.bodya.hackerrank.recursion

import java.util.*

// Complete the powerSum function below.
fun powerSum(x: Int, n: Int): Int {
    return calcCount(x, n, 1)
}

fun calcCount(x: Int, n: Int, num: Int): Int {
    val numToN = Math.pow(num.toDouble(), n.toDouble()).toInt()
    return when {
        numToN > x -> 0
        numToN == x -> 1
        else -> calcCount(x, n, num + 1) + calcCount(x - numToN, n, num + 1)
    }
}


fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val X = scan.nextLine().trim().toInt()

    val N = scan.nextLine().trim().toInt()

    val result = powerSum(X, N)

    println(result)
}