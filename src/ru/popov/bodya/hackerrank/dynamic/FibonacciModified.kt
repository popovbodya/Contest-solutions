package ru.popov.bodya.hackerrank.dynamic

import java.math.BigInteger
import java.util.*

// Complete the fibonacciModified function below.
fun fibonacciModified(t1: Int, t2: Int, n: Int): BigInteger {
    var tmp: BigInteger
    var num1 = BigInteger.valueOf(t1.toLong())
    var num2 = BigInteger.valueOf(t2.toLong())
    for (i in 0 until n - 1) {
        tmp = num1 + num2.pow(2)
        num1 = num2
        num2 = tmp
    }
    return num1
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    val t1T2n = scan.nextLine().split(" ")
    val t1 = t1T2n[0].trim().toInt()
    val t2 = t1T2n[1].trim().toInt()
    val n = t1T2n[2].trim().toInt()
    val result = fibonacciModified(t1, t2, n)
    println(result)
}
