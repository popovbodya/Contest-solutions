package ru.popov.bodya.hackerrank.strings

// Complete the superReducedString function below.
fun superReducedString(s: String): String {

    var j = 0
    val arr = s.toCharArray()
    for (i in 0 until arr.size) {
        if (j > 0 && arr[i] == arr[j - 1]) {
            j--
            continue
        }
        arr[j] = arr[i]
        j++
    }
    return if (j != 0) String(arr, 0, j) else "Empty String"
}

fun main(args: Array<String>) {
    val s = readLine()!!

    val result = superReducedString(s)

    println(result)
}
