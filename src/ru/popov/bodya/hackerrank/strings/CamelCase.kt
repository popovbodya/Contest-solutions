package ru.popov.bodya.hackerrank.strings

import java.util.*

// Complete the camelcase function below.
fun camelcase(s: String): Int = s.toCharArray().count { it.isUpperCase() } + 1

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    val s = scan.nextLine()
    val result = camelcase(s)
    println(result)
}