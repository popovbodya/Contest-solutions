package ru.popov.bodya.hackerrank.arrays;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class LeftRotation {

    private static final Scanner scanner = new Scanner(System.in);

    private static int[] rotLeft(int[] a, int d) {
        rotate(a, a.length - d);
        return a;
    }

    private static void rotate(int[] nums, int k) {
        final int rotateValue = k % nums.length;
        reverse(nums, 0, nums.length - 1);
        reverse(nums, 0, rotateValue - 1);
        reverse(nums, rotateValue, nums.length - 1);
    }

    private static void reverse(int[] numbers, int startPosition, int endPosition) {
        while (startPosition < endPosition) {
            int temp = numbers[startPosition];
            numbers[startPosition] = numbers[endPosition];
            numbers[endPosition] = temp;
            ++startPosition;
            --endPosition;
        }
    }

    public static void main(String[] args) throws IOException {
//        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
        BufferedWriter writer = new BufferedWriter(new PrintWriter(System.out));


        String[] nd = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nd[0]);
        int d = Integer.parseInt(nd[1]);
        int[] a = new int[n];

        String[] aItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int aItem = Integer.parseInt(aItems[i]);
            a[i] = aItem;
        }

        int[] result = rotLeft(a, d);

        for (int i = 0; i < result.length; i++) {
            writer.write(String.valueOf(result[i]));

            if (i != result.length - 1) {
                writer.write(" ");
            }
        }

        writer.newLine();
        writer.close();
        scanner.close();
    }


}



