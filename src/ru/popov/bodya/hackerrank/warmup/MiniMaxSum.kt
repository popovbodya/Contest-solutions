package ru.popov.bodya.hackerrank.warmup

import java.util.*

// Complete the miniMaxSum function below.
fun miniMaxSum(arr: Array<Int>) {
    val arrSum: Long = arr.sumElems()
    var min = Long.MAX_VALUE
    var max = Long.MIN_VALUE
    for (i in 0 until arr.size) {
        val tmp: Long = arrSum - arr[i]
        if (tmp < min) {
            min = tmp
        }
        if (arrSum - arr[i] > max) {
            max = tmp
        }
    }
    println("$min $max")
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val arr = scan.nextLine().split(" ").map { it.trim().toInt() }.toTypedArray()

    miniMaxSum(arr)
}

private fun Array<out Int>.sumElems(): Long {
    var sum = 0L
    for (element in this) {
        sum += element
    }
    return sum
}