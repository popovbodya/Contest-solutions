package ru.popov.bodya.hackerrank.warmup

import java.util.*

// Complete the plusMinus function below.
fun plusMinus(arr: Array<Int>): Unit {

    var positiveNumberCounter = 0.0
    var negativeNumberCounter = 0.0
    var zeroNumberCounter = 0.0

    for (i in 0 until arr.size) {
        val compareStatus = arr[i].compareTo(0)
        when {
            compareStatus > 0 -> positiveNumberCounter++
            compareStatus == 0 -> zeroNumberCounter++
            compareStatus < 0 -> negativeNumberCounter++
        }
    }

    val positiveResult = (positiveNumberCounter / arr.size).format(6)
    val negativeResult = (negativeNumberCounter / arr.size).format(6)
    val zeroResult = (zeroNumberCounter / arr.size).format(6)

    println(positiveResult)
    println(negativeResult)
    println(zeroResult)
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    val n = scan.nextLine().trim().toInt()
    val arr = scan.nextLine().split(" ").map { it.trim().toInt() }.toTypedArray()
    plusMinus(arr)
}

private fun Double.format(digits: Int): String = java.lang.String.format("%.${digits}f", this)