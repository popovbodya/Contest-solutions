package ru.popov.bodya.hackerrank.warmup

import java.util.*

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    val n = scan.nextLine().trim().toInt()
    val arr = Array(n) { Array(n) { 0 } }

    for (i in 0 until n) {
        arr[i] = scan.nextLine().split(" ").map { it.trim().toInt() }.toTypedArray()
    }

    val result = diagonalDifference(arr)

    println(result)
}

// Complete the diagonalDifference function below.
private fun diagonalDifference(arr: Array<Array<Int>>): Int =
        Math.abs(calculateLeftDiagonalElemsSum(arr) - calculateRightDiagonalElemsSum(arr))

private fun calculateLeftDiagonalElemsSum(arr: Array<Array<Int>>): Int {
    return (0 until arr.size).sumBy { arr[it][it] }
}

private fun calculateRightDiagonalElemsSum(arr: Array<Array<Int>>): Int {
    return (0 until arr.size).sumBy { arr[it][arr.size - it - 1] }
}

