package ru.popov.bodya.hackerrank.warmup

import java.lang.StringBuilder
import java.util.*

// Complete the staircase function below.
fun staircase(n: Int): Unit {
    for(i in 0 until n) {
        val line: String = StringBuilder().apply {
            for (j in 0 until n - 1 - i) {
                append(' ')
            }
            for (j in n - 1 - i until n) {
                append('#')
            }
        }.toString()
        println(line)
    }
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    val n = scan.nextLine().trim().toInt()

    staircase(n)
}
