package ru.popov.bodya.hackerrank.warmup

import java.text.SimpleDateFormat
import java.util.*

/*
 * Complete the timeConversion function below.
 */
fun timeConversion(s: String): String {
    val simpleDateFormat = SimpleDateFormat("hh:mm:ssa", Locale.ENGLISH)
    val date = simpleDateFormat.parse(s)
    return SimpleDateFormat("HH:mm:ss").format(date)
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    val s = scan.nextLine()
    val result = timeConversion(s)
    println(result)
}
