package ru.popov.bodya.hackerrank.warmup

import java.util.*

// Complete the birthdayCakeCandles function below.
fun birthdayCakeCandles(arr: Array<Int>): Int {
    var counter = 0
    var max = 0
    for (i in 0 until arr.size) {
        if (arr[i] > max) {
            max = arr[i]
            counter = 1
        } else if (arr[i] == max) {
            counter++
        }
    }
    return counter
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    val arCount = scan.nextLine().trim().toInt()
    val ar = scan.nextLine().split(" ").map { it.trim().toInt() }.toTypedArray()
    val result = birthdayCakeCandles(ar)
    println(result)
}