package ru.popov.bodya.hackerrank.implementation

import java.util.*

// Complete the breakingRecords function below.
fun breakingRecords(scores: Array<Int>): Array<Int> {

    var lowest = scores[0]
    var highest = scores[0]
    var lowestCount = 0
    var highestCount = 0
    for (score in scores) {
        if (score < lowest) {
            lowest = score
            lowestCount++

        } else if (score > highest) {
            highest = score
            highestCount++
        }
    }
    return arrayOf(highestCount, lowestCount)
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    val n = scan.nextLine().trim().toInt()
    val scores = scan.nextLine().split(" ").map { it.trim().toInt() }.toTypedArray()
    val result = breakingRecords(scores)
    println(result.joinToString(" "))
}