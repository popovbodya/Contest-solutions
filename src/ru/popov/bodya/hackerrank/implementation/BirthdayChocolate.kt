package ru.popov.bodya.hackerrank.implementation

// Complete the birthday function below.
fun birthday(s: Array<Int>, d: Int, m: Int): Int {

    var count = 0
    var currSum = 0
    for (i in 0 until s.size - (m - 1)) {
        for (j in i until m + i) {
            currSum += s[j]
        }
        if (currSum == d) count++
        currSum = 0
    }

    return count
}

fun main(args: Array<String>) {
    val n = readLine()!!.trim().toInt()
    val s = readLine()!!.trimEnd().split(" ").map { it.toInt() }.toTypedArray()
    val dm = readLine()!!.trimEnd().split(" ")
    val d = dm[0].toInt()
    val m = dm[1].toInt()
    val result = birthday(s, d, m)
    println(result)
}