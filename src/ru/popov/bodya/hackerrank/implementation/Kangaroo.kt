package ru.popov.bodya.hackerrank.implementation

import java.util.*
import kotlin.math.sign

// Complete the kangaroo function below.
fun kangaroo(x1: Int, v1: Int, x2: Int, v2: Int): String =
        if (isSignPositive(x1, v1, x2, v2) && isEquationTrue(x1, v1, x2, v2)) "YES" else "NO"

private fun isEquationTrue(x1: Int, v1: Int, x2: Int, v2: Int): Boolean = ((x1 - x2) % (v2 - v1) == 0)

private fun isSignPositive(x1: Int, v1: Int, x2: Int, v2: Int): Boolean =
        (x1 - x2).sign * (v2 - v1).sign > 0

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val x1V1X2V2 = scan.nextLine().split(" ")

    val x1 = x1V1X2V2[0].trim().toInt()

    val v1 = x1V1X2V2[1].trim().toInt()

    val x2 = x1V1X2V2[2].trim().toInt()

    val v2 = x1V1X2V2[3].trim().toInt()

    val result = kangaroo(x1, v1, x2, v2)

    println(result)
}
