package ru.popov.bodya.hackerrank.implementation

import java.math.BigInteger
import java.util.*

// Complete the extraLongFactorials function below.
fun extraLongFactorials(n: Int): Unit {
    println(factorialIterative(n))
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    val n = scan.nextLine().trim().toInt()
    extraLongFactorials(n)
}


private fun factorialIterative(n: Int): BigInteger {
    var result = BigInteger.ONE
    var i: BigInteger = BigInteger.ONE
    val count = BigInteger(n.toString(10))
    while (i <= count) {
        result = result.multiply(i)
        i++
    }
    return result
}

