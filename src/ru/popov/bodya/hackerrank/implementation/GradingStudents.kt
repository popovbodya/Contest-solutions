package ru.popov.bodya.hackerrank.implementation

import java.util.*

/*
 * Complete the gradingStudents function below.
 */
fun gradingStudents(grades: Array<Int>): Array<Int> {
    for (i in 0 until grades.size) {
        if (grades[i] >= 38) {
            if ((5 - grades[i] % 5) < 3)
                grades[i] = grades[i] + (5 - grades[i] % 5)
        }
    }
    return grades

}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    val n = scan.nextLine().trim().toInt()
    val grades = Array<Int>(n, { 0 })
    for (gradesItr in 0 until n) {
        val gradesItem = scan.nextLine().trim().toInt()
        grades[gradesItr] = gradesItem
    }
    val result = gradingStudents(grades)
    println(result.joinToString("\n"))
}
