package ru.popov.bodya.hackerrank.sorting

import java.util.*

// Complete the insertionSort function below.
fun insertionSort2(n: Int, arr: Array<Int>): Unit {
    for (i in 1 until n) {
        val pivot = arr[i]
        var j = i - 1
        while (j >= 0 && arr[j] > pivot) {
            arr[j + 1] = arr[j]
            j--
        }
        arr[j + 1] = pivot
        arr.forEach { print("$it ") }
        println()
    }
}

fun insertionSort1(n: Int, arr: Array<Int>) {
    for (i in n - 1 downTo 1) {
        val pivot = arr[i]
        var j = i - 1
        while (j >= 0 && arr[j] > pivot) {
            arr[j + 1] = arr[j]
            j--
            arr.forEach { print("$it ") }
            println()
        }
        if (arr[j + 1] != pivot) {
            arr[j + 1] = pivot
            arr.forEach { print("$it ") }
        }
    }
}


fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    val n = scan.nextLine().trim().toInt()
    val arr = scan.nextLine().split(" ").map { it.trim().toInt() }.toTypedArray()
    insertionSort2(n, arr)
}
