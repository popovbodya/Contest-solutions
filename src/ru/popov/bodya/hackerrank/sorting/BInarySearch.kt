package ru.popov.bodya.hackerrank.sorting

import java.util.*


// Complete the introTutorial function below.
fun introTutorial(v: Int, arr: Array<Int>): Int {
    return binarySearchInt(arr, v)
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    val V = scan.nextLine().trim().toInt()
    val n = scan.nextLine().trim().toInt()
    val arr = scan.nextLine().split(" ").map { it.trim().toInt() }.toTypedArray()
    val result = introTutorial(V, arr)
    println(result)
}

private fun binarySearchInt(arr: Array<Int>, key: Int): Int {
    var left = 0
    var right = arr.size - 1
    while (right - left > 1) {
        val middle = left + (right - left) / 2
        if (arr[middle] >= key)
            right = middle
        else
            left = middle

    }
    return right
}