package ru.popov.bodya.hackerrank.sorting

import java.util.*

// Complete the quickSort function below.
fun quickSort(arr: Array<Int>): Array<Int> {
    quickSort(arr, 0, arr.size - 1)
    return arr;
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    val n = scan.nextLine().trim().toInt()
    val arr = scan.nextLine().split(" ").map { it.trim().toInt() }.toTypedArray()
    val result = quickSort(arr)
    println(result.joinToString(" "))
}

private fun quickSort(array: Array<Int>, low: Int, high: Int) {
    var left = low
    var right = high
    val mid = (low + high) / 2
    val pivot = array[mid]
    while (left <= right) {
        while (array[left] < pivot) {
            left++
        }
        while (array[right] > pivot) {
            right--
        }
        if (left <= right) {
            swap(array, left, right)
            left++
            right--
        }
    }

    if (low < right) {
        quickSort(array, low, right)
    }
    if (left < high) {
        quickSort(array, left, high)
    }
}

private fun swap(array: Array<Int>, a: Int, b: Int) {
    val temp = array[a]
    array[a] = array[b]
    array[b] = temp
}
