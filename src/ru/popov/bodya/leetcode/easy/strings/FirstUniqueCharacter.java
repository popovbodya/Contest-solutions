package ru.popov.bodya.leetcode.easy.strings;

import java.util.HashMap;
import java.util.Map;

public class FirstUniqueCharacter {

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.firstUniqChar("loveleetcode"));
    }

    private static class Solution {

        private static final int ALL_CHARS_COUNT = 26;
        private static final int MAX_CHAR_INDEX = 97;

        private int slow(String s) {
            final Map<Character, Integer> characters = new HashMap<>();
            final char[] chars = s.toCharArray();
            for (char ch : chars) {
                characters.compute(ch, (character, integer) -> {
                    if (integer == null) {
                        return 1;
                    } else {
                        return integer + 1;
                    }
                });
            }
            for (int i = 0; i < chars.length; i++) {
                final Integer count = characters.get(chars[i]);
                if (count == 1) {
                    return i;
                }
            }
            return -1;
        }

        public int firstUniqChar(String s) {
            char[] chArr = s.toCharArray();
            int[] keep = new int[ALL_CHARS_COUNT];
            for (char ch : chArr) {
                keep[ch - MAX_CHAR_INDEX]++;
            }
            for (int i = 0; i < chArr.length; i++) {
                if (keep[chArr[i] - MAX_CHAR_INDEX] == 1)
                    return i;
            }
            return -1;
        }
    }
}
