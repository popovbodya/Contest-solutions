package ru.popov.bodya.leetcode.easy.strings;

public class ReverseString {

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.reverseString("123"));
    }

    private static class Solution {
        private String reverseString(String s) {
            final char[] chars = s.toCharArray();
            int i = 0;
            int j = chars.length - 1;
            while (i < j) {
                swap(chars, i, j);
                i++;
                j--;
            }
            return String.valueOf(chars);
        }

        private void swap(char arr[], int pos1, int pos2) {
            char t = arr[pos1];
            arr[pos1] = arr[pos2];
            arr[pos2] = t;
        }
    }
}
