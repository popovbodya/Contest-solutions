package ru.popov.bodya.leetcode.easy.strings;

public class StringToInteger {

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.myAtoi("42"));
        System.out.println(solution.myAtoi("       -42"));
        System.out.println(solution.myAtoi("4193 with words"));
        System.out.println(solution.myAtoi("words and 987"));
        System.out.println(solution.myAtoi("-91283472332"));
        System.out.println(solution.myAtoi("91283472332"));
        System.out.println(solution.myAtoi("-2147483649"));
        System.out.println(solution.myAtoi("2147483648"));
    }

    public static class Solution {

        private static final int RADIX = 10;

        private int myAtoi(String str) {

            int result = 0;
            int i = 0;
            boolean negative = false;

            while (i < str.length() && Character.isSpaceChar(str.charAt(i))) {
                i++;
            }

            if (i < str.length() && str.charAt(i) == '-') {
                negative = true;
                i++;
            } else if (i < str.length() && str.charAt(i) == '+') {
                i++;
            }

            while (i < str.length()) {
                char ch = str.charAt(i);
                if (!Character.isDigit(ch)) {
                    break;
                }
                if (isFutureNumberBiggerThanMax(result, ch)) {
                    return negative ? Integer.MIN_VALUE : Integer.MAX_VALUE;
                }
                result = result * RADIX + Character.digit(ch, RADIX);
                i++;
            }
            return negative ? -result : result;
        }

        private boolean isFutureNumberBiggerThanMax(int answer, char ch) {
            return answer > Integer.MAX_VALUE / RADIX || answer == Integer.MAX_VALUE / RADIX
                   && Character.digit(ch, RADIX) > Integer.MAX_VALUE % RADIX;
        }
    }
}
