package ru.popov.bodya.leetcode.easy.strings;

public class ValidAnagram {

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.isAnagram("rat", "car"));
    }

    private static class Solution {

        private static final int ALL_CHARS_COUNT = 26;
        private static final int FIRST_CHAR_INDEX = 97;

        private boolean isAnagram(String s, String t) {
            if (s.length() != t.length()) {
                return false;
            }
            final int[] charCountArrayS = getCharCountArray(s.toCharArray());
            final int[] charCountArrayT = getCharCountArray(t.toCharArray());
            for (int i = 0; i < ALL_CHARS_COUNT; i++) {
                if (charCountArrayS[i] != charCountArrayT[i]) {
                    return false;
                }
            }
            return true;
        }

        private int[] getCharCountArray(char[] arr) {
            int[] charsCount = new int[ALL_CHARS_COUNT];
            for (char ch : arr) {
                charsCount[ch - FIRST_CHAR_INDEX]++;
            }
            return charsCount;
        }
    }

}
