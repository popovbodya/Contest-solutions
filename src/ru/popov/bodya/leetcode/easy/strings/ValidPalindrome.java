package ru.popov.bodya.leetcode.easy.strings;

public class ValidPalindrome {

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.IsPalindromeSlow("A man, a plan, a canal: Panama"));
        ;
        System.out.println(solution.IsPalindromeSlow("race a car"));
        ;
        System.out.println(solution.IsPalindromeSlow("0P"));
        ;
    }

    private static class Solution {
        private boolean IsPalindromeSlow(String s) {
            final char[] chars = s.replaceAll("[^a-zA-Z0-9]", "").toCharArray();
            int i = 0;
            int j = chars.length - 1;
            while (i < j) {
                if (i < j && Character.toLowerCase(chars[i]) != Character.toLowerCase(chars[j])) {
                    return false;
                }
                i++;
                j--;
            }
            return true;
        }

        private boolean isPalindrome(String s) {
            int i = 0;
            int j = s.length() - 1;
            while (i < j) {
                while (i < j && !Character.isLetterOrDigit(s.charAt(i))) {
                    i++;
                }
                while (i < j && !Character.isLetterOrDigit(s.charAt(j))) {
                    j--;
                }
                if (i < j) {
                    if (Character.toLowerCase(s.charAt(i)) != Character.toLowerCase(s.charAt(j))) {
                        return false;
                    }
                    i++;
                    j--;
                }

            }
            return true;
        }
    }

}
