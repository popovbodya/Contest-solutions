package ru.popov.bodya.leetcode.easy.strings;

public class StrImpl {

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.strStr("a", ""));
        System.out.println(solution.strStr("", "a"));
        System.out.println(solution.strStr("", ""));
    }

    private static class Solution {
        public int strStr(String haystack, String needle) {
            return indexOf(haystack, needle);
        }

        private int indexOf(String source, String target) {

            if (source.length() == 0) {
                return target.length() != 0 ? -1 : 0;
            } else {
                if (target.length() == 0) {
                    return 0;
                }
            }

            char first = target.charAt(0);
            int max = (source.length() - target.length());

            for (int i = 0; i <= max; i++) {
                if (source.charAt(i) != first) {
                    while (++i <= max && source.charAt(i) != first) ;
                }

                if (i <= max) {
                    int j = i + 1;
                    int end = j + target.length() - 1;
                    for (int k = 1; j < end && source.charAt(j) == target.charAt(k); j++, k++) ;

                    if (j == end) {
                        return i;
                    }
                }
            }
            return -1;
        }
    }
}
