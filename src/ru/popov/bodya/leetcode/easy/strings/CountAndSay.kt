package ru.popov.bodya.leetcode.easy.strings

private class Solution {
    private fun recursiveCountAndSay(n: Int): String {
        if (n == 1) return "1"

        val charArray = recursiveCountAndSay(n - 1).toCharArray()
        var sameCharCounter = 1;
        var ch = charArray[0]
        val sb = StringBuilder()

        return sb.apply {
            for (i in 1 until charArray.size) {
                if (charArray[i] == ch) {
                    sameCharCounter++
                } else {
                    append(sameCharCounter)
                    append(ch)
                    ch = charArray[i]
                    sameCharCounter = 1
                }
            }
            append(sameCharCounter)
            append(ch)
        }
                .toString()
    }

    fun countAndSay(n: Int): String {
        return iterativeCountAndSay(n)
    }

    private fun iterativeCountAndSay(n: Int): String {

        if (n == 0) return ""

        var result = StringBuilder("1")
        for (i in 1 until n) {
            result = updateBuilderOnIteration(result)
        }

        return result.toString()
    }

    private fun updateBuilderOnIteration(prev: StringBuilder): StringBuilder {
        val current = StringBuilder()
        var sameCharCounter = 1
        var sameChar = prev[0]
        for (j in 1 until prev.length) {
            if (prev[j] == sameChar) {
                sameCharCounter++;
            } else {
                current.append("$sameCharCounter$sameChar")
                sameCharCounter = 1
                sameChar = prev[j]
            }
        }
        current.append("$sameCharCounter$sameChar")
        return current
    }
}

fun main(args: Array<String>) {
    val solution = Solution()
    println(solution.countAndSay(3))
}