package ru.popov.bodya.leetcode.easy.strings;

public class ReverseInteger {

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.reverse(120));
    }

    private static class Solution {
        private int reverse(int x) {
            final char[] chars = String.valueOf(x).toCharArray();

            int i;
            int j = chars.length - 1;
            if (chars[0] == '-') {
                i = 1;
            } else {
                i = 0;
            }
            while (i < j) {
                swap(chars, i, j);
                i++;
                j--;
            }
            int value;
            try {
                value = Integer.parseInt(String.valueOf(chars), 10);
            } catch (NumberFormatException e) {
                value = 0;
            }
            return value;
        }

        private void swap(char arr[], int pos1, int pos2) {
            char t = arr[pos1];
            arr[pos1] = arr[pos2];
            arr[pos2] = t;
        }
    }


}
