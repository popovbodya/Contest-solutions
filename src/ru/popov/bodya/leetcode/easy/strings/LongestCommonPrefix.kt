package ru.popov.bodya.leetcode.easy.strings

import java.lang.StringBuilder


class LongestCommonPrefixSolution {

    fun longestCommonPrefix(strs: Array<String>): String {

        if (strs.isEmpty() || strs[0].isEmpty()) {
            return ""
        }
        val baseString = strs[0]
        var j = 0
        val sb = StringBuilder()
        var shouldContinue = true
        while (j < baseString.length) {
            for (i in 1 until strs.size) {
                if (j >= strs[i].length || baseString[j] != strs[i][j]) {
                    shouldContinue = false
                    break
                } else {
                    shouldContinue = true
                }
            }
            if (shouldContinue) {
                sb.append(baseString[j])
                j++
            } else {
                break
            }
        }

        return sb.toString()
    }
}

fun main(args: Array<String>) {
    val solution = LongestCommonPrefixSolution()
    println(solution.longestCommonPrefix(arrayOf("flower", "flow", "flight")))
    println(solution.longestCommonPrefix(arrayOf("dog","racecar","car")))
    println(solution.longestCommonPrefix(arrayOf("a")))
    println(solution.longestCommonPrefix(arrayOf("")))
    println(solution.longestCommonPrefix(arrayOf("", "")))
    println(solution.longestCommonPrefix(arrayOf()))
    println(solution.longestCommonPrefix(arrayOf("a", "a")))
    println(solution.longestCommonPrefix(arrayOf("b", "bo", "bod", "body", "bodya")))
}