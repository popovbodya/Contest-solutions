package ru.popov.bodya.leetcode.easy.arrays;

public class RotateMatrix {

    public static void main(String[] args) {

        int[][] matrix = new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        Solution solution = new Solution();
        solution.rotateWithTranspose(matrix);
    }

    private static class Solution {

        private void rotate(int[][] matrix) {
            rotateWithTranspose(matrix);
        }

        private void clockwiseRotate(int[][] arr) {
            int len = arr.length;
            for (int i = 0; i < len / 2; i++) {
                for (int j = len - 1 - i; j > i; j--) {
                    int tmp = arr[i][j];
                    arr[i][j] = arr[len - 1 - j][i];
                    arr[len - 1 - j][i] = arr[len - i - 1][len - 1 - j];
                    arr[len - 1 - i][len - 1 - j] = arr[j][len - 1 - i];
                    arr[j][len - 1 - i] = tmp;
                }
            }
        }

        private void rotateWithTranspose(int[][] arr) {
            transpose(arr);
            for (int[] row : arr) {
                reverseRow(row);
            }
            print(arr);
        }

        private void transpose(int[][] arr) {
            int tmp;
            for (int i = 0; i < arr.length; i++) {
                for (int j = i; j < arr.length; j++) {
                    tmp = arr[i][j];
                    arr[i][j] = arr[j][i];
                    arr[j][i] = tmp;
                }
            }
        }

        private void reverseRow(int[] row) {
            int i = 0;
            int j = row.length - 1;
            while (i < j) {
                swap(row, i, j);
                i++;
                j--;
            }
        }

        private void swap(int[] a, int pos1, int pos2) {
            int t = a[pos1];
            a[pos1] = a[pos2];
            a[pos2] = t;
        }

        private void print(int[][] arr) {
            for (int[] row : arr) {
                for (int elem : row) {
                    System.out.print(elem + " ");
                }
                System.out.println();
            }
        }
    }
}
