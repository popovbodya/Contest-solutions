package ru.popov.bodya.leetcode.easy.arrays;

import java.util.Arrays;

public class RotateArray {

    public static void main(String[] args) {
        Solution solution = new Solution();
        solveQuiz(solution, new int[]{-1, -100, 3, 99}, 2);
    }

    private static void solveQuiz(Solution solution, int[] nums, int k) {
        solution.rotate(nums, k);
        Arrays.stream(nums).forEach(num -> System.out.print(num + " "));
        System.out.println();
    }

    private static class Solution {
        private void rotate(int[] nums, int k) {
            final int rotateValue = k % nums.length;
            reverse(nums, 0, nums.length - 1);
            reverse(nums, 0, rotateValue - 1);
            reverse(nums, rotateValue, nums.length - 1);
        }

        private void reverse(int[] numbers, int startPosition, int endPosition) {
            while (startPosition < endPosition) {
                int temp = numbers[startPosition];
                numbers[startPosition] = numbers[endPosition];
                numbers[endPosition] = temp;
                ++startPosition;
                --endPosition;
            }
        }
    }
}
