package ru.popov.bodya.leetcode.easy.arrays;

import java.util.Arrays;

/**
 * Given a non-empty array of digits representing a non-negative integer, plus one to the integer.
 * <p>
 * The digits are stored such that the most significant digit is at the head of the list, and each element in the array contain a single digit.
 * <p>
 * You may assume the integer does not contain any leading zero, except the number 0 itself.
 */
public class PlusOne {

    public static void main(String[] args) {
        Solution solution = new Solution();
        final int[] plusOne = solution.plusOne(new int[]{1});
        Arrays.stream(plusOne).forEach(number -> System.out.print(number + " "));
    }


    private static class Solution {
        private int[] plusOne(int[] digits) {
            int i = digits.length - 1;
            while (i >= 0 && digits[i] == 9) {
                i--;
            }

            int[] result;
            if (i == -1) {
                result = new int[digits.length + 1];
                result[0] = 1;
                Arrays.fill(result, 1, result.length, 0);
                return result;
            } else {
                result = digits;
                result[i]++;
                Arrays.fill(result, i + 1, result.length, 0);
            }
            return result;
        }
    }
}
