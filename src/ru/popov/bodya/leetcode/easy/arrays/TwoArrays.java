package ru.popov.bodya.leetcode.easy.arrays;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;

/**
 * Given two arrays, write a function to compute their intersection.
 * <p>
 * Example:
 * Given nums1 = [1, 2, 2, 1], nums2 = [2, 2], return [2, 2].
 */
public class TwoArrays {

    public static void main(String[] args) {
        Solution solution = new Solution();
        final int[] intersect = solution.intersect(new int[]{1, 2, 2, 3, 1}, new int[]{3, 2, 2, 4});
        Arrays.stream(intersect).forEach(number -> System.out.print(number + " "));
    }

    private static class Solution {
        private int[] intersect(int[] nums1, int[] nums2) {
            final int[] result = new int[Math.min(nums1.length, nums2.length)];
            Arrays.sort(nums1);
            Arrays.sort(nums2);

            int i = 0, j = 0, k = 0;
            while (i < nums1.length && j < nums2.length) {
                if (nums1[i] == nums2[j]) {
                    result[k] = nums1[i];
                    k++;
                    i++;
                    j++;
                } else if (nums1[i] < nums2[j]) {
                    i++;
                } else {
                    j++;
                }
            }
            return Arrays.copyOf(result, k);
        }
    }
}
