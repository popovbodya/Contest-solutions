package ru.popov.bodya.leetcode.easy.arrays;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Given an array of integers, return indices of the two numbers such that they add up to a specific target.
 * <p>
 * You may assume that each input would have exactly one solution, and you may not use the same element twice.
 */
public class TwoSum {

    public static void main(String[] args) {
        Solution solution = new Solution();
        final int[] twoSum = solution.twoSum(new int[]{2, 7, 11, 15}, 9);
        Arrays.stream(twoSum).forEach(number -> System.out.print(number + " "));
    }

    private static class Solution {
        private int[] twoSum(int[] nums, int target) {
            Map<Integer, Integer> map = new HashMap<>();
            for (int i = 0; i < nums.length; i++) {
                final int mapKey = target - nums[i];
                final Integer mapValue = map.get(mapKey);
                if (mapValue != null) {
                    return new int[]{map.get(mapKey), i};
                } else {
                    map.put(nums[i], i);
                }
            }
            return new int[0];
        }
    }
}
