package ru.popov.bodya.leetcode.easy.arrays;

import java.util.Arrays;

/**
 * Say you have an array for which the ith element is the price of a given stock on day i.
 * <p>
 * Design an algorithm to find the maximum profit. You may complete as many transactions as you like (i.e., buy one and sell one share of the stock multiple times).
 */
public class Stock {

    public static void main(String[] args) {
        Solution solution = new Solution();
        solveWithArray(solution, new int[]{7, 1, 5, 3, 6, 4});
        solveWithArray(solution, new int[]{1, 2, 3, 4, 5});
        solveWithArray(solution, new int[]{7, 6, 4, 3, 1});
        solveWithArray(solution, new int[]{1, 4, 2});
    }

    private static void solveWithArray(Solution solution, int[] prices) {
        System.out.println(solution.maxProfit(prices));
    }

    private static class Solution {

        private int maxProfit(int[] prices) {
            int maxProfit = 0;
            int currentStockIndex = 0;
            boolean isInStockNow = false;
            for (int i = 0; i < prices.length - 1; i++) {
                if (isInStockNow) {
                    if (prices[i] >= prices[i + 1]) {
                        maxProfit += prices[i] - prices[currentStockIndex];
                        isInStockNow = false;
                    }
                } else if (prices[i] < prices[i + 1]) {
                    isInStockNow = true;
                    currentStockIndex = i;
                }
            }

            if (isInStockNow) {
                maxProfit += prices[prices.length - 1] - prices[currentStockIndex];
            }
            return maxProfit;
        }

        private int leetcodeMaxProfit(int[] prices) {
            int maxProfit = 0;
            for (int i = 1; i < prices.length; i++) {
                if (prices[i] > prices[i - 1])
                    maxProfit += prices[i] - prices[i - 1];
            }
            return maxProfit;
        }
    }
}
