package ru.popov.bodya.leetcode.easy.arrays;

/**
 * Given a non-empty array of integers, every element appears twice except for one. Find that single one.
 * <p>
 * Note:
 * <p>
 * Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?
 */
public class SingleNumber {

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.singleNumber(new int[]{1, 1, 1, 1}));
    }

    private static class Solution {
        private int singleNumber(int[] nums) {
            int result = 0;
            for (int number : nums) {
                result ^= number;
            }
            return result;
        }
    }
}
