package ru.popov.bodya.leetcode.easy.arrays;

import java.util.Arrays;

/**
 * Given a sorted array nums, remove the duplicates in-place such that each element appear only once and return the new length.
 * <p>
 * Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.
 */
public class RemoveDuplicates {

    public static void main(String[] args) {
        final Solution solution = new Solution();

        int[] numbers = new int[]{Integer.MIN_VALUE, 0, 1};
        final int uniqueNumbersLength = solution.removeDuplicates(numbers);
        System.out.println(uniqueNumbersLength);
        Arrays.stream(numbers).limit(uniqueNumbersLength).forEach(number -> System.out.print(number + " "));
        System.out.println();
        numbers = new int[]{Integer.MIN_VALUE, 0, 1};
        final int leetcodeLength = solution.leetcodeRemoveDuplicates(numbers);
        System.out.println(leetcodeLength);
        Arrays.stream(numbers).limit(leetcodeLength).forEach(number -> System.out.print(number + " "));
        System.out.println();

    }

    private static class Solution {
        private int removeDuplicates(int[] nums) {
            if (nums.length == 0) return 0;
            int currentUniqueIndex = 1;
            int uniqueNumbersCounter = 1;
            for (int i = 0; i < nums.length - 1; i++) {
                if (nums[i] != nums[i + 1]) {
                    nums[currentUniqueIndex] = nums[i + 1];
                    ++uniqueNumbersCounter;
                    ++currentUniqueIndex;
                }
            }
            return uniqueNumbersCounter;
        }

        private int leetcodeRemoveDuplicates(int[] nums) {
            if (nums.length == 0) return 0;
            int i = 0;
            for (int j = 1; j < nums.length; j++) {
                if (nums[j] != nums[i]) {
                    i++;
                    nums[i] = nums[j];
                }
            }
            return i + 1;
        }
    }
}
