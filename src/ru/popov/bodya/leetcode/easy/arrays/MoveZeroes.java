package ru.popov.bodya.leetcode.easy.arrays;

import java.util.Arrays;

/**
 * Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.
 * Input: [0,1,0,3,12]
 * Output: [1,3,12,0,0]
 */
public class MoveZeroes {

    public static void main(String[] args) {
        Solution solution = new Solution();
        final int[] numbers = new int[]{0, 1, 0, 3, 12};
        solution.leetCodeMoveZeroes(numbers);
        Arrays.stream(numbers).forEach(number -> System.out.print(number + " "));
    }

    private static class Solution {
        private void moveZeroes(int[] nums) {
            int i = 0, j = 0;
            while (i < nums.length) {
                if (nums[i] != 0) {
                    nums[j] = nums[i];
                    j++;
                }
                i++;
            }
            while (j < nums.length) {
                nums[j] = 0;
                j++;
            }
        }

        private void leetCodeMoveZeroes(int[] nums) {
            for (int lastNonZeroFoundAt = 0, cur = 0; cur < nums.length; cur++) {
                if (nums[cur] != 0) {
                    swap(nums, lastNonZeroFoundAt++, cur);
                }
            }
        }

        private void swap(int[] a, int pos1, int pos2) {
            int t = a[pos1];
            a[pos1] = a[pos2];
            a[pos2] = t;
        }
    }
}
