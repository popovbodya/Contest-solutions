package ru.popov.bodya.leetcode.easy.arrays;

import java.util.HashSet;
import java.util.Set;

public class ValidSudoku {

    public static void main(String[] args) {
        Solution solution = new Solution();
        final char[][] chars = {
                {'0', '1', '2', '3', '4', '5', '6', '7', '8'},
                {'9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'},
                {'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q'},
                {'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'},
                {'0', '1', '2', '3', '4', '5', '6', '7', '8'},
                {'9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'},
                {'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q'},
                {'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'},
                {'0', '1', '2', '3', '4', '5', '6', '7', '8'},
        };

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.println(solution.square(chars, i, j));
            }
        }
    }

    private static class Solution {

        private static final int ROWS_COUNT = 9;
        private static final int COLUMN_COUNT = 9;
        private static final int SQUARE_ELEMENTS_COUNT = 9;
        private static final int SQUARE_WIDTH = 3;
        private static final int SQUARE_HEIGHT = 3;

        private boolean isValidSudoku(char[][] board) {

            // check rows
            for (int i = 0; i < ROWS_COUNT; i++) {
                if (containsDuplicate(board[i])) {
                    return false;
                }
            }

            // check columns
            for (int i = 0; i < COLUMN_COUNT; i++) {
                if (containsDuplicate(column(board, i))) {
                    return false;
                }
            }

            for (int i = 0; i < SQUARE_HEIGHT; i++) {
                for (int j = 0; j < SQUARE_WIDTH; j++) {
                    if (containsDuplicate(square(board, i, j))) {
                        return false;
                    }
                }
            }

            return true;

        }

        private char[] square(char[][] mas, int rowIndex, int columnIndex) {
            final char[] square = new char[SQUARE_ELEMENTS_COUNT];
            int realRowIndex = rowIndex * SQUARE_HEIGHT;
            int realColumnIndex = columnIndex * SQUARE_WIDTH;
            int k = 0;
            for (int i = 0; i < SQUARE_HEIGHT; i++) {
                for (int j = 0; j < SQUARE_WIDTH; j++) {
                    square[k++] = mas[realRowIndex + i][realColumnIndex + j];
                }
            }
            return square;
        }

        private char[] column(char[][] mas, int index) {
            final int size = mas.length;
            final char[] column = new char[size];
            for (int i = 0; i < size; i++) {
                column[i] = mas[i][index];
            }
            return column;
        }

        private boolean containsDuplicate(char[] nums) {
            Set<Character> set = new HashSet<>();
            for (char num : nums) {
                if (num == '.') {
                    continue;
                }
                if (set.contains(num)) {
                    return true;
                } else {
                    set.add(num);
                }
            }
            return false;
        }
    }
}
