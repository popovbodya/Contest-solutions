package ru.popov.bodya.codeforces.div3.round481.solutions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.StringTokenizer;


public class A {

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        TaskA solver = new TaskA();
        solver.solve(in, out);
        out.close();
    }

    private static class TaskA {

        private void solve(InputReader in, PrintWriter out) {

            final int size = in.nextInt();
            final Deque<Integer> result = new ArrayDeque<>(size);
            final int[] numbers = new int[size];
            final int[] numbersCount = new int[1001];

            for (int i = 0; i < size; i++) {
                numbers[i] = in.nextInt();
                numbersCount[i] = 0;
            }

            for (int i = size - 1; i >= 0; i--) {
                final int number = numbers[i];
                if (numbersCount[number] == 0) {
                    result.push(number);
                }
                numbersCount[number] += 1;
            }

            out.println(result.size());
            result.forEach(integer -> out.print(integer + " "));
        }
    }

    private static class InputReader {

        private static final int BUF_SIZE = 32768;
        private BufferedReader mBufferedReader;
        private StringTokenizer mTokenizer;

        private InputReader(InputStream stream) {
            mBufferedReader = new BufferedReader(new InputStreamReader(stream), BUF_SIZE);
            mTokenizer = null;
        }

        private String next() {
            while (mTokenizer == null || !mTokenizer.hasMoreTokens()) {
                try {
                    mTokenizer = new StringTokenizer(mBufferedReader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return mTokenizer.nextToken();
        }

        private int nextInt() {
            return Integer.parseInt(next());
        }
    }

}
