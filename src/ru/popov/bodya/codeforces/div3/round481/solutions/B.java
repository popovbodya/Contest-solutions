package ru.popov.bodya.codeforces.div3.round481.solutions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class B {

    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        TaskB solver = new TaskB();
        solver.solve(in, out);
        out.close();
    }

    private static class TaskB {

        private void solve(InputReader in, PrintWriter out) {

            in.nextInt();
            char[] chars = in.next().toCharArray();

            int count = 0;
            int localCount = 0;

            for (char ch : chars) {
                if (ch == 'x') {
                    localCount++;
                    if (localCount > 2) {
                        count++;
                    }
                } else {
                    localCount = 0;
                }
            }
            System.out.println(count);
        }
    }


    private static class InputReader {

        private static final int BUF_SIZE = 32768;
        private BufferedReader mBufferedReader;
        private StringTokenizer mTokenizer;

        private InputReader(InputStream stream) {
            mBufferedReader = new BufferedReader(new InputStreamReader(stream), BUF_SIZE);
            mTokenizer = null;
        }

        private String next() {
            while (mTokenizer == null || !mTokenizer.hasMoreTokens()) {
                try {
                    mTokenizer = new StringTokenizer(mBufferedReader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return mTokenizer.nextToken();
        }

        private int nextInt() {
            return Integer.parseInt(next());
        }
    }

}
