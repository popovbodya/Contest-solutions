package ru.popov.bodya.codeforces.div3.round481.solutions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class C {


    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        TaskC solver = new TaskC();
        solver.solve(in, out);
        out.close();
    }

    private static class TaskC {
        private void solve(InputReader in, PrintWriter out) {

            final int n = in.nextInt();
            final int m = in.nextInt();

            final long[] homes = new long[n + 1];

            long lastValue = 0;
            homes[0] = 0;
            for (int i = 1; i <= n; i++) {
                lastValue = lastValue + in.nextLong();
                homes[i] = lastValue;
            }
            for (int i = 0; i < m; i++) {
                long messageTo = in.nextLong();
                int index = binarySearchLong(homes, messageTo);
                out.println(index + " " + (messageTo - homes[index - 1]));

            }
        }

        private static int binarySearchLong(long[] arr, long key) {
            int left = 0;
            int right = arr.length - 1;

            while (right - left > 1) {
                int middle = left + (right - left) / 2;
                if (arr[middle] >= key) {
                    right = middle;
                } else {
                    left = middle;
                }
            }
            return right;
        }
    }


    private static class InputReader {

        private static final int BUF_SIZE = 32768;
        private BufferedReader mBufferedReader;
        private StringTokenizer mTokenizer;

        private InputReader(InputStream stream) {
            mBufferedReader = new BufferedReader(new InputStreamReader(stream), BUF_SIZE);
            mTokenizer = null;
        }

        private String next() {
            while (mTokenizer == null || !mTokenizer.hasMoreTokens()) {
                try {
                    mTokenizer = new StringTokenizer(mBufferedReader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return mTokenizer.nextToken();
        }

        private int nextInt() {
            return Integer.parseInt(next());
        }

        private long nextLong() {
            return Long.parseLong(next());
        }
    }

}
